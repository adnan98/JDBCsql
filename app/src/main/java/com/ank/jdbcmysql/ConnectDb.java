package com.ank.jdbcmysql;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Admin on 10/26/2017.
 */

public class ConnectDb {
    public Connection getConnection(){
        Connection conn=null;
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String cUrl = "jdbc:mysql://192.168.137.1:3306/db_android";
            String cUser = "adnan";
            String cPass = "123";
            conn = DriverManager.getConnection(cUrl,cUser,cPass);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(conn!=null){
            Log.d("Connection","CONNECT");
        }else{
            Log.d("Connection","FAIL");
        }

        return conn;
    }
}
